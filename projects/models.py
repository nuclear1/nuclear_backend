import uuid

from django.db import models


class Project(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=200)
    slug = models.SlugField(max_length=200)
    description = models.TextField(blank=True)
    path = models.TextField()
    creation_date = models.DateTimeField(auto_now_add=True)
    last_update = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class OpenProject(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    project = models.OneToOneField('projects.Project', on_delete=models.CASCADE, blank=True, null=True)
    index = models.IntegerField(unique=True)

    open_date = models.DateTimeField(auto_now_add=True)
    last_save = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.project) if self.project else f"dashboard n°{self.id}"

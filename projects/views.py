from django.shortcuts import render
from rest_framework import permissions
from rest_framework import mixins
from rest_framework import generics

from rest_framework.viewsets import ModelViewSet

from projects.models import Project, OpenProject
from projects.serializers import ProjectSerializer, OpenProjectSerializer


class ProjectViewset(ModelViewSet):
    queryset = Project.objects.order_by('creation_date')
    serializer_class = ProjectSerializer
    permission_classes = [permissions.AllowAny]


class OpenProjectViewset(ModelViewSet):
    queryset = OpenProject.objects.order_by('index')
    serializer_class = OpenProjectSerializer
    permission_classes = [permissions.AllowAny]

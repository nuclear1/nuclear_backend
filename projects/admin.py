from django.contrib import admin

from projects.models import Project, OpenProject


@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ("name",)}


@admin.register(OpenProject)
class OpenProjectAdmin(admin.ModelAdmin):
    ordering = ('index',)
    list_display = ('__str__', 'index',)

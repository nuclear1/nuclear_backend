from rest_framework import serializers

from projects import models


class ProjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Project
        fields = '__all__'


class OpenProjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.OpenProject
        fields = '__all__'
        depth = 1

import json

from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.views import APIView
import os

from core.serializers import Directory


class Directories(APIView):
    def get(self, request, format=None):
        return Response(sorted(os.listdir('/')))

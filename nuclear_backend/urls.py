"""nuclear_backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
import os
import subprocess
from multiprocessing.context import Process

from django.contrib import admin
from django.urls import path, include

from nuclear_backend import settings

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('core.urls')),
    path('projects/', include('projects.urls')),
]

if settings.ELECTRON_STARTUP:
    os.environ["NODE_ENV"] = "production"
    p = Process(target=subprocess.call, args=(['node', '../nuclear_frontend/.electron-nuxt/index.js'],))
    p.start()
